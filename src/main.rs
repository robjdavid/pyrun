use std::process::Command;
use std::env;
use std::path::PathBuf;
use std::iter;
use std::process::exit;

fn main() {
    let args = env::args().skip(1);
    let myexe = env::current_exe().expect("Cannot determine executable name");
    let mut script = PathBuf::from(myexe);

    script.set_extension("py");
    if !script.exists() {
        script.set_extension("");
        if !script.exists() {
            panic!("Cannot find {} or {} to launch",
                script.to_string_lossy(),
                script.with_extension("py").to_string_lossy())
        }
    }

    let python = env::var("PYRUN_PYTHON").unwrap_or_else(|_| "python".to_string());
    let script_name = script.to_str().unwrap().to_string();
    let launch_args = iter::once(script_name).chain(args);
    let mut child = Command::new(&python)
        .args(launch_args)
        .spawn()
        .expect(format!("Failed to launch {}",python).as_str());
    exit(child.wait().unwrap().code().expect("Terminated"));
}